<?php

class PageController extends \BaseController {
    
    public function __construct()
    {
        $this->beforeFilter('csrf', array('on' => ['store', 'destroy']));
        $this->beforeFilter('auth', array('on' => ['destroy']));
    }
    
    public function show($manga_id, $episode_id, $id){
        return View::make('episode.show')->with('manga', Manga::find($manga_id))
                ->with('episode', Episode::find($episode_id))
                ->with('page', Page::find($id));
    }
    
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($manga_id, $episode_id)
	{
        //var_dump($manga_id);
		if($manga = MangaHelper::ownsManga($manga_id)){
            $rules = array(
                'thumbnail' => 'image',
            );

            $validation = Validator::make(Input::all(), $rules);

            if ($validation->fails())
            {
                return Response::make($validation->errors->first(), 400);
            }
            $page = new Page;
            $last_num = Page::where('episode_id', '=', $episode_id)->max('num');
            $last_num = ($last_num) ? $last_num : 0;
            $page->num = $last_num;
            $page->image = Input::file('thumbnail');
            $page->episode_id = $episode_id;
            $page->save();
            
            if (Request::wantsJson())
            {
                return Response::json(array('success' =>  'true',
                                            'page_id' =>  $page->id,
                                            'page_num' => $page->num
                                           ));
            }
            else {
                return Redirect::action('MangaController@show', [$manga->id]);
            }
        }
        //return Redirect::action('MangaController@index');
        return Response::json(array('success' =>  'false',
                                            'manga_id' =>  $manga_id,
                                            'episode_id' => $episode_id
                                           ));
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($manga_id, $episode_id, $id)
	{
		if($manga = MangaHelper::ownsManga($manga_id)){
            if($pageCurrent = Page::findOrFail(Input::get('page_id'))){
                Page::where('episode_id', '=', $episode_id)
                      ->where('num', '>', $pageCurrent->num)->decrement('num', 1);
                $pageCurrent->image->delete();
                $pageCurrent->delete();
                return Response::json(array('success' =>  'true',));
            }
        }
	}
}
