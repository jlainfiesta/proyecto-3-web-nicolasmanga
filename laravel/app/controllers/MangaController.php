<?php

class MangaController extends \BaseController {

	public function __construct()
    {
        $this->beforeFilter('csrf', array('on' => ['store', 'update', 'destroy']));
        $this->beforeFilter('auth', array('except' => ['show', 'index']));
    }
    
    /**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$mangas = Manga::all();
        return View::make('manga.index')->with('manga_list', $mangas)->with('own', false);
	}
    
    public function indexOwn()
	{
		$mangas = Manga::where('user_id', '=', Auth::user()->id)->get();
        return View::make('manga.index')->with('manga_list', $mangas)->with('own', true);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('manga.create')->with('categories', Category::all());
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$manga = new Manga;
        $rules = array(
            'title'  => 'required',
            'description'  => 'required',
            'thumbnail' => 'image'
        );
        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails()) {
            // get the error messages from the validator
            $messages = $validator->messages();
            Input::flash();
            // redirect our user back to the form with the errors from the validator
            return Redirect::to('/#login-register')
                ->withErrors($validator);
        } else {
            $manga->title = Input::get('title');
            $manga->description = Input::get('description');
            
            
            $manga->thumb = Input::file('thumbnail');
            //Log::info('Thumbnail: '.var_dump($manga->thumb));
            
            $manga->user_id = Auth::user()->id;
            $manga->save();
            
            $cats = Input::get('categories');
            if(is_array($cats)){
                foreach($cats as $cat){
                    Category::find($cat)->mangas()->attach($manga->id);
                }
            }
            
            Session::flash('success', trans('manga.created'));
            if (Request::wantsJson())
            {
                return Response::json(array('success' => 'true',
                                            'manga_id' => $manga->id
                                           ));
            }
            else {
                return Redirect::action('MangaController@show', [$manga->id]);
            }
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $manga = Manga::find($id);
        if($manga){
            return View::make('manga.show')->with('manga', Manga::find($id));
        } else {
            return Redirect::to("/");
        }
		
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        if($manga = MangaHelper::ownsManga($id)){
            return View::make('manga.edit')->with('categories', Category::all())->with('manga', Manga::find($id));
        } else {
            return Redirect::to("/")->withErrors([trans('manga.no_permissions')]);
        }
		
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		if($manga = MangaHelper::ownsManga($id)){
            $rules = array(
                'title'  => 'required',
                'description'  => 'required',
                'thumbnail' => 'image'
            );
            $validator = Validator::make(Input::all(), $rules);
            if($validator->fails()) {
                // get the error messages from the validator
                $messages = $validator->messages();
                Input::flash();
                // redirect our user back to the form with the errors from the validator
                return Redirect::to('/')
                    ->withErrors($validator);
            } else {
                $manga->title = Input::get('title');
                $manga->description = Input::get('description');

                if(Input::file('thumbnail')){
                    $manga->thumb = Input::file('thumbnail');
                }
                
                $manga->save();
                
                //Remove other relationships
                foreach($manga->categories as $cat){
                    $manga->categories()->detach($cat->id);
                }
                
                //Add new categories
                $cats = Input::get('categories');
                if(is_array($cats)){
                    foreach($cats as $cat){
                        Category::find($cat)->mangas()->attach($manga->id);
                    }
                }

                
                if (Request::wantsJson())
                {
                    return Response::json(array('success' => 'true'
                                               ));
                }
                else {
                    Session::flash('success', trans('manga.updated'));
                    return Redirect::action('MangaController@edit', [$manga->id]);
                }
            }
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		if($manga = MangaHelper::ownsManga($id)){
            $manga->categories()->detach();
            $manga->thumb->delete();
            Manga::destroy($manga->id);
            Session::flash('success', trans('manga.deleted'));
            return Redirect::action('MangaController@index');
        }
	}


}
