<?php

class UserController extends BaseController {


	public function showSign(){
        
    }
    
    public function doSign() {
        if(Input::get('register_box') === 'register'){
            //Do register
            $rules = array(
                'username'             => 'required|unique:users',
                'password'         => 'required'
            );

            $validator = Validator::make(Input::all(), $rules);

            if ($validator->fails()) {
                // get the error messages from the validator
                $messages = $validator->messages();
                // redirect our user back to the form with the errors from the validator
                return Redirect::to('/#login-register')
                    ->withErrors($validator);
            } else {
                $user = new User;
                $user->username = Input::get('username');
                $user->password = Hash::make(Input::get('password'));
                $user->save();
                Auth::attempt(['username'=>Input::get('username'), 'password'=>Input::get('password')]);
                Session::flash('success', trans('general.registered', array('username'=>$user->username)) );
                return Redirect::to('/');
            }
            
        } else {
            //Do login
            if(Auth::attempt(['username'=>Input::get('username'), 'password'=>Input::get('password')])){
                Session::flash('success', trans('general.logged', array('username'=>Auth::user()->username)) );
                return Redirect::intended('/');
            } else {
                return Redirect::to('/#login-register')
                    ->withErrors([trans('general.login_error')]); 
            }
        }
        
	}
    
    public function doSignout(){
        Auth::logout();
        Session::flash('success', trans('general.loggedout'));
        return Redirect::to('/');
    }
    

}

