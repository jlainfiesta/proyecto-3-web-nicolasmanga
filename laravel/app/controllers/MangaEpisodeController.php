<?php

class MangaEpisodeController extends \BaseController {
    
    public function __construct()
    {
        $this->beforeFilter('csrf', array('on' => ['store', 'update', 'destroy']));
        $this->beforeFilter('auth', array('except' => ['show', 'index']));
    }
    
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($manga_id)
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($manga_id)
	{
        if($manga = MangaHelper::ownsManga($manga_id)){
            $last_episode = Episode::where('manga_id','=',$manga_id)->max('num');
            $last_episode = ($last_episode) ? $last_episode : 1;
            $newEpisode = new Episode;
            $newEpisode->title = trans('episode.episode')." #".$last_episode;
            $newEpisode->manga_id = $manga->id;
            $newEpisode->save();
            return Redirect::action('MangaEpisodeController@edit', [$manga->id, $newEpisode->id]);
        }
		return Redirect::to("MangaController@index")->withErrors([trans('manga.no_permissions')]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($manga_id, $id)
	{
        if($episode = Episode::find($id)){
            if($episode->pages->count() > 0){
                $first_page = $episode->pages()->take(1)->get();
                return Redirect::action('PageController@show', [$manga_id, $id, $first_page[0]->id]);
            }
            
        }
        return Redirect::to("/");
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($manga_id, $id)
	{
        if($manga = MangaHelper::ownsManga($manga_id)){
            return View::make('episode.edit')->with('manga', $manga)->with('episode', Episode::find($id));
        }
		return Redirect::to("MangaController@index")->withErrors([trans('manga.no_permissions')]);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($manga_id, $id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($manga_id, $id)
	{
		//
	}


}
