<?php 

class MangaHelper {

    //Private function to check for permissions
    public static function ownsManga($manga_id){
        if (!Auth::check())
            return false;
        $manga = Manga::find($manga_id);
        //If it's ours
        if($manga && $manga->user->id == Auth::user()->id){
            return $manga;   
        } else {
            return false;
        }
    }
    
    
}
?>