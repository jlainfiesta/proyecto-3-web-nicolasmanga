<?php

return array(

	'episode' => 'Episode',
    'edit' => 'Edit episode',
    'new' => 'New episode',
    'title' => 'Episode title:',
    'dropto' => 'Drop to',
    'addpage' => 'Add Page',
    'pages' => 'Pages',
    
);

?>