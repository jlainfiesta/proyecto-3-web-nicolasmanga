<?php

return array(

	'create_title' => 'Creating a whole new manga',
    'create_decription' => 'Are you ready to share your awesome creation with the world?',
    'title' => 'Manga title',
    'description' => 'Description',
    'categories' => 'Categories',
    'img_description' => '...an image speaks a thousand words',
    'create_new' => 'Create new manga!',
    'drop_image' => 'Drop image',
    'for_manga' => 'for manga thumb',
    'created' => 'New manga created',
    'edit' => 'Edit',
    'editing' => 'You\'re making changes to your manga',
    'update' => 'Update Manga',
    'updated' => 'Manga succesfully updated!',
    'delete' => 'Delete Manga',
    'deleted' => 'Manga successully deleted',
    'no_permissions' => 'You have no permissions for this manga',
    'new_episode' => 'New episode',
    'read' => 'Read Manga',
    'manga_own' => 'Your manga',
    'pages' => 'Pages',
    
);

?>