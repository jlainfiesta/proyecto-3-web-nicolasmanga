<?php

return array(
    'title' => 'NicolasManga',
	'login_register' => 'Login / Register',
    'login' => 'Login',
    'register' => 'Register',
    'username' => 'Username',
    'password' => 'Password',
    'new_account' => 'I\'m a new user, register an account.',
    'close' => 'Close',
    'logged' => '<strong>:username</strong> logged in.',
    'registered' => '<strong>:username</strong> registered.',
    'login_error' => 'Username / password do not match.',
    'logout' => 'Logout',
    'loggedout' => 'Log out successful.',
    'back' => 'Go back'
    
);

?>