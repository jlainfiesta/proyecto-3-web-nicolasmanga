<?php 
class CategoryTableSeeder extends Seeder {

    public function run()
    {
        
        $user = User::create(array('username' => 'admin', 'password' => Hash::make('hola')));

        $cat1 = Category::create(array('name' => 'Action'));
        $cat2 = Category::create(array('name' => 'Horror'));
        $cat3 = Category::create(array('name' => 'Fantasy'));
        
        $manga = Manga::create(array('title'=> 'Maduro Hits Tokio', 'description' => 'Lorem ipsum', 'user_id'=>$user->id));
        $manga->categories()->attach($cat1->id);
        
    }

}
?>