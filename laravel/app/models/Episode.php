<?php 

use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

class Episode extends Eloquent {
    use EloquentTrait;
    
    protected $fillable = ["title"];
	
    public function pages(){
        return $this->hasMany('Page');   
    }
    public function manga(){
        return $this->belongsTo('Manga');   
    }
    
    public function getThumb(){
        if($this->pages->count()>0){
            $firstP = $this->pages()->take(1)->get();
           // var_dump($firstP);
            return $firstP[0]->image->url('thumb');
        }
        return false;
    }

}

?>