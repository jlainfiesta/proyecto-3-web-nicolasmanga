<?php 

use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

class Page extends Eloquent implements StaplerableInterface {
    use EloquentTrait;
    
    protected $fillable = ["image"];
	
    public function __construct(array $attributes = array()) {
        $this->hasAttachedFile('image', [
            'styles' => [
            'large' => '800x1200',
            'medium' => '500x750',
            'thumb' => '320x480'
            ]
        ]);
        parent::__construct($attributes);
    }
    
    public function episode(){
        return $this->belongsTo('Episode');
    }
    
    public function getPrev(){
        $pageprev = Page::where('episode_id', '=', $this->episode_id)->where('id', '<', $this->id)->take(1)->get();
        if(count($pageprev)>0){
            //return var_dump($pageprev);
            return $pageprev[0];
        } else {
            return false;
        }
        
    }
    public function getNext(){
        $pageprev = Page::where('episode_id', '=', $this->episode_id)->where('id', '>', $this->id)->take(1)->get();
        if(count($pageprev)>0){
            //return var_dump($pageprev);
            return $pageprev[0];
        } else {
            return false;
        }
    }
    
}

?>