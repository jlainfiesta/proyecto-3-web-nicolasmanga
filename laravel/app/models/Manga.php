<?php 

use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

class Manga extends Eloquent implements StaplerableInterface {
    use EloquentTrait;
    
    protected $guarded = ["id"];
	
    public function __construct(array $attributes = array()) {
        $this->hasAttachedFile('thumb', [
            'styles' => [
            'large' => '500x750',
            'small' => '400x600'
            ]
        ]);
        parent::__construct($attributes);
    }
    
    public function user(){
        return $this->belongsTo('User');   
    }
    
    public function episodes(){
        return $this->hasMany('Episode');   
    }
    
    public function categories(){
        return $this->belongsToMany('Category'); 
    }

}

?>