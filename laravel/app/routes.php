<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return Redirect::to('manga');
});

//User Routes
Route::post('user/sign', array('before' => 'csrf', 'uses' => 'UserController@doSign'));
Route::post('user/signout', array('before' => 'csrf', 'uses' => 'UserController@doSignout'));
Route::get('login', function(){
    return Redirect::to("/#login-register");   
});

//Define extra routes for resources
Route::get('manga/own', 'MangaController@indexOwn');

//Route::post('manga/{manga_id}/episode/{episode_id}/add-page', 'MangaEpisodeController@addPage');
//Route::post('manga/{manga_id}/episode/{episode_id}/remove-page', 'MangaEpisodeController@removePage');

//Define Manga resource
Route::resource('manga', 'MangaController');
Route::resource('manga.episode', 'MangaEpisodeController', array('except' => array('create')) );
Route::resource('manga.episode.page', 'PageController', array('only' => array('store', 'destroy', 'show')) );

//Create Manga Create Routes
//Route::get('create', 'MangaController@showCreate');
//Route::get('create/manga', 'MangaController@showCreateNew');
//Route::post('create/manga', 'MangaController@doCreateNew');
//
//    //Display different if author or viewer
//    Route::get('manga/{manga_id}', 'MangaController@showManga');
//
//    //Creates a dummy and shows the edit page
//    Route::post('manga/{manga_id}/new_episode', array('before' => 'csrf', 'uses' => 'EpisodeController@doAddEpisode'));
//    Route::get('manga/{manga_id}/{episode_id}/edit', 'MangaController@showAddEpisode');
//
//        //Edit options
//        Route::post('manga/{manga_id}/episode-{episode_id}/edit/change-name',
//                    array('before' => 'csrf', 'uses' => 'EpisodeController@doChangeName'));
//        //Requires image only, num is calculated in backend
//        Route::post('manga/{manga_id}/episode-{episode_id}/edit/add-page',
//                    array('before' => 'csrf', 'uses' => 'EpisodeController@doAddPage'));
//        //Remove page
//        Route::post('manga/{manga_id}/episode-{episode_id}/{page_num}/remove',
//                    array('before' => 'csrf', 'uses' => 'PageController@doRemove'));
//        //Change page image
//        Route::post('manga/{manga_id}/episode-{episode_id}/page-{page_num}/change-image',
//                    array('before' => 'csrf', 'uses' => 'PageController@doChangeImage'));