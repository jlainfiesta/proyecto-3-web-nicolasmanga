@extends('layout.base')

@section('title', trans('manga.update'))

@section('page-script')
    <script>require(['page/create_content']);</script>
@stop

@section('content')


<div class="page_header">
    <a href="{{ URL::previous() }}" class="back-link"><i class="glyphicon glyphicon-chevron-left"></i> {{trans('general.back')}}</a>
    <h1>{{trans('manga.editing')}}</h1>
</div>

  {{-- Form::open(array('action' => ['MangaController@update', $manga->id], 'class'=>'create_manga_form dropzone', 'id'=>'create-manga-form', 'method'=>'PUT')) --}}
{{ Form::model($manga, array('route' => array('manga.update', $manga->id), 'method' => 'PUT', 'class'=>'create_manga_form dropzone', 'id'=>'create-manga-form')) }}
   <div class="manga_meta">
       {{ Form::label('title', trans('manga.title')) }}
       {{ Form::text('title', null, array('placeholder'=>trans('manga.title'))) }}
       
       {{ Form::label('description', trans('manga.description')) }}
       {{ Form::textarea('description', null, array('placeholder'=>trans('manga.description'), 'rows'=>3)) }}
       
       {{ Form::label('categories[]', trans('manga.categories')) }}
       
       @foreach($categories as $key => $cat)
           <label for="cat-{{$cat->id}}">
                {{-- Tick the boxes accordingly --}}
                {{ Form::checkbox('categories[]', $cat->id, ($manga->categories->contains($cat->id)) ? true : false, ['id'=>'cat-'.$cat->id]) }}
                {{$cat->name}}
            </label>
       @endforeach
       <br>
       {{ Form::submit(trans('manga.update'), ['class'=>'btn_primary']) }}
    
   </div>
   <div class="manga_thumb">
      {{ Form::label('thumbnail', trans('manga.img_description')) }}
      {{-- Form::file('thumbnail') --}}
       
      <div class="drop_upload_file dz-message" data-dz-message>
        <i class="glyphicon glyphicon-hand-up upload_icon"></i><br>
        <label for="manga_thumb" class="control-label">{{trans('manga.drop_image')}} <br> {{trans('manga.for_manga')}} </label><br>
        <small>or click here</small>
      </div>
      
      <div class="dropzone-previews"></div>
   </div>
{{ Form::close() }}
           
{{Form::open(['action'=>['MangaController@destroy', $manga->id], 'method'=>'DELETE', 'class'=>'delete_form'])}}
    <i class="glyphicon glyphicon-trash"></i> {{ Form::submit(trans('manga.delete'), ['class'=>'btn-default']) }}
{{ Form::close() }}          
@stop
