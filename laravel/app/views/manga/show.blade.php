@extends('layout.base')

@section('title', trans('manga.create_new'))

@section('page-script')
    <script>require(['page/create_content']);</script>
@stop

@section('content')

<div class="page_header">
   <div class="manga_info">
           <a href="{{ action('MangaController@index') }}" class="back-link"><i class="glyphicon glyphicon-chevron-left"></i> {{trans('general.back')}}</a>
        <h1>{{$manga->title}}
           @if(MangaHelper::ownsManga($manga->id))
            <a href="{{action('MangaController@edit', [$manga->id])}}" class="btn-default"><i class="glyphicon glyphicon-pencil"></i> {{trans('manga.edit')}}</a>
            @endif
        </h1>

       {{-- <ul class="stars_list">
            <li><i class="glyphicon glyphicon-star"></i></li>
            <li><i class="glyphicon glyphicon-star"></i></li>
            <li><i class="glyphicon glyphicon-star"></i></li>
        </ul> --}}
        <p>{{$manga->description}}</p>

        <strong>Categories</strong>  
        <ul class="stats">
        @foreach($manga->categories as $key => $cat)
           <li>
               {{ $cat->name }}
           </li> 
        @endforeach 
        </ul>
   </div>
     
    <div class="manga_thumb">
        <img src="{{ $manga->thumb->url('large') }}" >
    </div>
                    
</div>

<section class="episode_list">
   @foreach($manga->episodes as $episode)
        <article class="episode_element">
            <div class="manga_title">
                <h4><a href="{{action('MangaEpisodeController@show', [$manga->id, $episode->id])}}">{{$episode->title}}</a></h4>
                <a href="{{action('MangaEpisodeController@show', [$manga->id, $episode->id])}}"><img src="{{$episode->getThumb()}}" /></a>
            </div>

            <ul class="stats">
                {{-- <li><i class="glyphicon glyphicon-calendar"></i> 12/10/2014</li> --}}
                <li>{{trans('episode.pages')}}: {{$episode->pages->count()}}</li>
                {{-- <li><i class="glyphicon glyphicon-comment"></i> 9</li> --}}
            </ul>
            @if(MangaHelper::ownsManga($manga->id))
            <a href="{{action('MangaEpisodeController@edit', [$manga->id, $episode->id])}}" class="btn_primary">{{trans('episode.edit')}}</a>
            @endif

        </article>
    @endforeach
</section>


@stop