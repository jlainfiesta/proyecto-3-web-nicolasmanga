@extends('layout.base')

@section('title', trans('manga.create_new'))

@section('page-script')
    <script>require(['page/create_content']);</script>
@stop

@section('content')


<div class="page_header">
    <a href="{{ URL::previous() }}" class="back-link"><i class="glyphicon glyphicon-chevron-left"></i> {{trans('general.back')}}</a>
    <h1>Creating a whole new manga<br /><small>Are you ready to share your awesome creation with the world?</small></h1>
</div>

  {{ Form::open(array('action' => 'MangaController@store', 'class'=>'create_manga_form dropzone', 'id'=>'create-manga-form')) }}
   <div class="manga_meta">
       {{ Form::label('title', trans('manga.title')) }}
       {{ Form::text('title', Input::old('title'), array('placeholder'=>trans('manga.title'))) }}
       
       {{ Form::label('description', trans('manga.description')) }}
       {{ Form::textarea('description', Input::old('description'), array('placeholder'=>trans('manga.description'), 'rows'=>3)) }}
       
       {{ Form::label('categories[]', trans('manga.categories')) }}
       
       @foreach($categories as $key => $cat)
           <label for="cat-{{$cat->id}}">
                {{ Form::checkbox('categories[]', $cat->id, false, ['id'=>'cat-'.$cat->id]) }}
                {{$cat->name}}
            </label>
       @endforeach
       <br>
       {{ Form::submit(trans('manga.create_new'), ['class'=>'btn_primary']) }}
    
   </div>
   <div class="manga_thumb">
      {{ Form::label('thumbnail', trans('manga.img_description')) }}
      {{-- Form::file('thumbnail') --}}
       
      <div class="drop_upload_file dz-message" data-dz-message>
        <i class="glyphicon glyphicon-hand-up upload_icon"></i><br>
        <label for="manga_thumb" class="control-label">{{trans('manga.drop_image')}} <br> {{trans('manga.for_manga')}} </label><br>
        <small>or click here</small>
      </div>
      
      <div class="dropzone-previews"></div>
       
       {{-- Form::file('thumbnail', Input::old('thumbnail')) --}}
   </div>
{{ Form::close() }}
            
@stop
