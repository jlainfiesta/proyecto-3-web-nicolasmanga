@extends('layout.base')

@section('title', ($own) ? trans('manga.manga_own') : trans('manga.read'))


@section('content')


<div class="page_header">
    <h1>
        @if($own)
            {{trans('manga.manga_own')}}
            <a href="{{action('MangaController@create')}}" class="btn_primary">{{trans('manga.create_new')}}</a>
        @else
            {{trans('manga.read')}}
        @endif
    </h1>
</div>

<section class="manga_list">
   @foreach($manga_list as $manga)
        <article class="manga_series">
            <div class="manga_title">
                <h3><a href="{{action('MangaController@show', [$manga->id])}}">{{$manga->title}}</a></h3>
                <a href="{{action('MangaController@show', [$manga->id])}}"><img src="{{ $manga->thumb->url('large') }}" /></a>
            </div>
            @if($own)
                {{Form::open(['action'=>['MangaEpisodeController@store', $manga->id]])}}
                    {{ Form::submit(trans('episode.new'), ['class'=>'btn-default new_episode']) }}
                {{ Form::close() }} 
            @endif
            <ul class="stats">
                <li>{{$manga->episodes->count()}} episodes</li>
            </ul>
        </article> 
   @endforeach
     
</section>


@stop