<!DOCTYPE html>
<html>
    <head>
        <title>
            @if($__env->yieldContent('title'))
                @yield('title') &lang; {{trans('general.title')}}
            @else
                {{trans('general.title')}}
            @endif
            
        </title>
        <link href='http://fonts.googleapis.com/css?family=Rokkitt:400,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,300italic,700' rel='stylesheet' type='text/css'>
        {{ HTML::style('styles/global.css') }}
    </head>
    <body>
        <header>
            <nav class="navbar" role="navigation">
                <div class="navbar_header">
                  <button type="button" class="navbar_toggle collapsed" data-toggle="collapse" data-target="#main_menu">
                    <span class="sr_only">Toggle navigation</span>
                    <i class="glyphicon glyphicon-chevron-down"></i>
                  </button>
                  <a class="navbar_brand" href="index.html">{{trans('general.title')}}</a>
                </div>

                <div class="collapse navbar_collapse navbar_collapsed" id="main_menu">
                    <ul class="nav">
                        <li><a href="{{url('manga/own')}}">Create</a></li>
                        <li><a href="{{url('manga/all')}}">Read</a></li>
                    </ul>
                    @if(!Auth::check())
                        @include('layout.guest_nav')
                    @else
                        @include('layout.user_nav')
                    @endif
                </div><!-- /.navbar-collapse -->
            </nav>
        </header>
        <section class="main_content">
           @if(Session::has('success'))
               <div class="alert success">
                   {{ Session::get('success') }}
               </div>
            @endif
            
            @yield('content')
        </section>
        @if($__env->yieldContent('page-script'))
            {{ HTML::script('scripts/lib/require.js', ['data-main'=>'/scripts/main']) }}
            @yield('page-script')
        @endif
        
        @if(!Auth::user())
           {{ HTML::script('scripts/lib/require.js', ['data-main'=>'/scripts/main']) }}
            <script>require(['page/guest_start']);</script>
        @endif
    </body>
</html>