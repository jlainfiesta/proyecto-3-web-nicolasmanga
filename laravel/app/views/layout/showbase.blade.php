<!DOCTYPE html>
<html>
    <head>
        <title>
            @if($__env->yieldContent('title'))
                @yield('title') &lang; {{trans('general.title')}}
            @else
                {{trans('general.title')}}
            @endif
            
        </title>
        <link href='http://fonts.googleapis.com/css?family=Rokkitt:400,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,300italic,700' rel='stylesheet' type='text/css'>
        {{ HTML::style('styles/global.css') }}
    </head>
    <body class="show_theme">

        <section class="main_content">
           @if(Session::has('success'))
               <div class="alert success">
                   {{ Session::get('success') }}
               </div>
            @endif
            
            @yield('content')
        </section>
        @if($__env->yieldContent('page-script'))
            {{ HTML::script('scripts/lib/require.js', ['data-main'=>'/scripts/main']) }}
            @yield('page-script')
        @endif
    </body>
</html>