
{{ Form::open(array('action' => 'UserController@doSignout', 'class'=>'logout_form')) }}
    {{ Form::submit(trans('general.logout'), ['class'=>'btn btn-default']) }}
{{ Form::close() }}
<ul class="nav nav_right">
    <li>
        <i class="glyphicon glyphicon-user"></i>
        {{Auth::user()->username}}
    </li>
</ul>
