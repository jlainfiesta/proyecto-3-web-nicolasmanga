<ul class="nav nav_right">
    <li>
        <a href="#login-register" class="modal_trigger" id="login_trigger" data-toggle="modal" data-target="#login-register">
            <i class="glyphicon glyphicon-user"></i> {{ trans('general.login_register') }}
        </a>
    </li>
</ul>
<div class="modal_area" id="login-register" role="dialog">
<div class="modal_dialog">
  <div class="modal_content">
    <header>
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr_only">{{trans('general.close')}}</span></button>
      <h4 class="modal-title" id="myModalLabel">{{ trans('general.login_register') }}</h4>
    </header>
    {{ Form::open(array('action' => 'UserController@doSign', 'class'=>'login_form')) }}	
        <div class="modal_body">
           @if ($errors->has())
                <div class="alert error">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>		
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="form-group">
                <!-- name -->
                {{ Form::label('username', trans('general.username')) }}
                {{ Form::text('username', Input::old('username'), array('placeholder'=>trans('general.username'), 'required'=>'required')) }}
            </div>
            <div class="form-group">
                {{ Form::label('password', trans('general.password')) }}
                {{ Form::password('password', ['placeholder'=>trans('general.password'), 'required'=>'required']); }}
            </div>
            <div class="form-group">
               <label for="register_box" class="checkbox_label">
                   {{ Form::checkbox('register_box', 'register') }}
                   {{ trans('general.new_account') }}
               </label>
            </div>
        </div>
        <footer>
            {{ Form::submit(trans('general.login'), ['class'=>'btn btn-default', 'data-registerlang'=>trans('general.register')]) }}
        </footer>
    {{ Form::close() }}
  </div>
</div>
</div>



