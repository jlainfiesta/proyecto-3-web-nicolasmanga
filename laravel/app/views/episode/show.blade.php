@extends('layout.showbase')

@section('title', $episode->title)

@section('page-script')
    <script>require(['page/edit_episode']);</script>
@stop

@section('content')

<div class="page_header">
    <a href="{{ action('MangaController@show', [$manga->id]) }}" class="back-link"><i class="glyphicon glyphicon-chevron-left"></i>{{$manga->title}}</a>
    <strong>{{$episode->title}}</strong>
    {{-- <nav class="manga_pagination">
        <ol>
            <li class="active"><a href="#p1">1</a></li>
            <li><a href="#p1">2</a></li>
            <li><a href="#p1">3</a></li>
            <li><a href="#p1">4</a></li>
        </ol>
    </nav> --}}
    <br>
</div>

<section class="manga_book single clearfix">
   <nav class="manga_quick_nav">
       @if($page->getPrev())
           <a href="{{action('PageController@show', [$manga->id, $episode->id, $page->getPrev()->id])}}" class="back"><i class="glyphicon glyphicon-chevron-right"></i></a>
       @endif
       @if($page->getNext())
           <a href="{{action('PageController@show', [$manga->id, $episode->id, $page->getNext()->id])}}" class="next"><i class="glyphicon glyphicon-chevron-left"></i></a>
       @endif
    </nav>
    <div class="manga_page content_page left_page">
        <div class="manga_img_wrap">
            <img src="{{$page->image->url('large')}}" alt="">
        </div>
    </div>
</section>
  
            
@stop
