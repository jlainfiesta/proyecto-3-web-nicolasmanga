@extends('layout.base')

@section('title', trans('episode.edit'))

@section('page-script')
    <script>require(['page/edit_episode']);</script>
@stop

@section('content')


<div class="page_header">
    <a href="{{ action('MangaController@show', [$manga->id]) }}" class="back-link"><i class="glyphicon glyphicon-chevron-left"></i> {{$manga->title}}</a>
    {{ Form::model($episode, array('route' => array('manga.episode.update', $manga->id, $episode->id), 'method' => 'PUT', 'class'=>'update_episode')) }}
        <h1>{{trans('episode.edit')}}
        {{-- Form::label('title', trans('episode.title')) --}}
        {{ Form::text('title', null, array('placeholder'=>trans('episode.title'))) }}
        </h1>
    {{ Form:: close() }}
</div>

<section class="page_edit_list">
    {{ Form::open(array('action' => ['PageController@store', $manga->id, $episode->id], 'id'=>'page_list')) }}
       <li class="add-new-page">
            <div class="drop_upload_file">
                <i class="glyphicon glyphicon-plus-sign upload_icon"></i><br>
                <label for="manga_thumb" class="control-label">{{trans('episode.dropto')}} <br> {{trans('episode.addpage')}}</label>
            </div>
        </li>
        
        <article class="page page-template">
            <div class="thumbnail">
                <img data-dz-thumbnail>
            </div>
            <div class="meta">
                <span data-dz-name> <em data-dz-size></em></span>
                <strong class="error text-danger" data-dz-errormessage></strong>
                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                  <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                </div>
                <a class="btn-default delete" href="javascript:undefined;" data-dz-remove>
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Delete</span>
                </a>
            </div>
        </article>
    {{ Form::close() }}

</section>
  
            
@stop
