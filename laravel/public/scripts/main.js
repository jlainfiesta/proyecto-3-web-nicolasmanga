requirejs.config({
    "baseUrl": "/scripts/lib",
    "paths": {
      "app": "../app",
      "jquery": "//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min",
      "handlebars": "handlebars",
      "dropzone": "dropzone"
    },
    schim: {
        handlebars: {
            exports: 'Handlebars'
        }
    }
});

