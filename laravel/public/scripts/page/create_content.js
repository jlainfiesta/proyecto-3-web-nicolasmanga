require(['main'], function() {
    require(['jquery', 'dropzone'], function($, Dropzone) {

        Dropzone.autoDiscover = false;
        
        $( document ).ready(function(){
            var dropZone = new Dropzone("#create-manga-form", {
                  autoProcessQueue: false,
                  uploadMultiple: false,
                  parallelUploads: 1,
                  maxFiles: 1,
                  previewsContainer: ".dropzone-previews",
                  addRemoveLinks: true,
                  acceptedFiles: "image/*",
                  paramName: "thumbnail",
                  thumbnailWidth: "400",
                  thumbnailHeight: "250",
                  clickable: [".drop_upload_file"],
                  init: function(){
                      this.on("maxfilesexceeded", function(file) {
                        this.removeAllFiles();
                        this.addFile(file);
                      });
                  }
            });
            $("#create-manga-form").on("submit", function(e){
                e.preventDefault();
                e.stopPropagation();
                dropZone.processQueue();
            });
        });
        
//        Dropzone.options.createMangaForm = { 
//
//          autoProcessQueue: false,
//          uploadMultiple: false,
//          parallelUploads: 1,
//          maxFiles: 1,
//          previewsContainer: ".dropzone-previews",
//          addRemoveLinks: true,
//          acceptedFiles: "image/*",
//          paramName: "thumbnail",
//          thumbnailWidth: "400",
//          thumbnailHeight: "250",
//          clickable: [".drop_upload_file"],
//
//          // The setting up of the dropzone
//          init: function() {
//            var myDropzone = this;
//
//            // First change the button to actually tell Dropzone to process the queue.
//            this.element.querySelector("input[type=submit]").addEventListener("click", function(e) {
//              // Make sure that the form isn't actually being sent.
//              e.preventDefault();
//              e.stopPropagation();
//              myDropzone.processQueue();
//            });
//
//            // Listen to the sendingmultiple event. In this case, it's the sendingmultiple event instead
//            // of the sending event because uploadMultiple is set to true.
//            this.on("sendingmultiple", function() {
//              // Gets triggered when the form is actually being sent.
//              // Hide the success button or the complete form.
//            });
//            this.on("successmultiple", function(files, response) {
//              // Gets triggered when the files have successfully been sent.
//              // Redirect user or notify of success.
//            });
//            this.on("errormultiple", function(files, response) {
//              // Gets triggered when there was an error sending the files.
//              // Maybe show form again, and notify user of error
//            });
//            this.on("maxfilesexceeded", function(file) {
//                this.removeAllFiles();
//                this.addFile(file);
//            });
//            this.on('success', function(file, response){
//                //Redirect
//                alert(response);
//            });
//          }
//
//        }

    });
});