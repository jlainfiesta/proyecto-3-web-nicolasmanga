require(['main'], function() {
    require(['jquery'], function($) {
        $( document ).ready(function(){
            
            //TODO: detect hashchange
            if(window.location.hash == "#login-register"){
                alert("here");
                $("#login_trigger").click();
            }
            
            //Close dialog
            $(".modal_dialog").on("click", "button.close", function(){
                var $dialog = $(this).parent().parent().parent();
                $dialog.slideUp(300, function(){
                    $dialog.parent().fadeOut(200);
                });
            });
            //Open Dialog
            $(".modal_trigger").on("click", function(){
                var $dialog_whole = $($(this).data("target"));
                $dialog_whole.fadeIn(200, function(){
                    $dialog_whole.find(".modal_dialog").slideDown(300);   
                });

            })
            //Navbar toggle
            $(".navbar_toggle").on("click", function(){
                var $menu = $($(this).data("target"));
                if($(this).hasClass("collapsed")){
                    $(this).removeClass("collapsed");
                    $menu.removeClass("navbar_collapsed");
                }else {
                    $(this).addClass("collapsed");
                    $menu.addClass("navbar_collapsed");
                }
            });

        });
    });
});